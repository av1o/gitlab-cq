/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package core

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/gitlab-cq/pkg/gitlab"
	"os"
)

func WriteReports(reports []gitlab.Report, file string) error {
	data, err := json.MarshalIndent(reports, "", "\t")
	if err != nil {
		log.WithError(err).Error("failed to convert reports to json")
		return err
	}
	log.Infof("writing report to file: %s", file)
	// write the json to a file
	err = os.WriteFile(file, data, 0644)
	if err != nil {
		log.WithError(err).Error("failed to write reports to file")
		return err
	}
	return nil
}
