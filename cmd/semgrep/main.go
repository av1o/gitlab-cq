/*
 *    Copyright 2022 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/gitlab-cq/pkg/generic"
	"os"
	"path/filepath"
	"strings"
)

const (
	FileRaw      = "/tmp/results.sarif"
	FileArtifact = "codequality-semgrep.json"
)

var configPath = filepath.Join("/", "rules")

func main() {
	args := fmt.Sprintf("semgrep ci --sarif --disable-version-check --metrics=off --no-rewrite-rule-ids --suppress-errors --output %s --config %s || true", FileRaw, configPath)
	// support the gitlab sast env vars
	if val := os.Getenv("SECURE_LOG_LEVEL"); strings.ToLower(val) == "debug" {
		args = args + " --verbose"
		log.SetLevel(log.DebugLevel)
	}
	if err := generic.RunSARIF(FileRaw, FileArtifact, "sh", "-c", args); err != nil {
		log.WithError(err).Fatal("failed to run")
	}
}
