/*
 *    Copyright 2023 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package main

import (
	"flag"
	"github.com/owenrumney/go-sarif/sarif"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/gitlab-cq/internal/core"
	"gitlab.com/av1o/gitlab-cq/pkg/generic"
)

func main() {
	fFileIn := flag.String("input", "", "path to the SARIF file")
	fFileOut := flag.String("output", "", "path to export the GitLab CodeQuality report")

	flag.Parse()

	// read the sarif report
	log.WithField("path", *fFileIn).Info("importing SARIF report")
	report, err := sarif.Open(*fFileIn)
	if err != nil {
		log.WithError(err).Fatal("failed to read report")
	}
	// convert it to a gitlab report
	glRep := generic.AsGitLab(report)
	// write the gitlab report out
	// to the requested file
	log.WithField("path", *fFileOut).Info("exporting GitLab CodeQuality report")
	if err := core.WriteReports(glRep, *fFileOut); err != nil {
		log.WithError(err).Fatal("failed to write report")
	}
}
