# GitLab CQ

This project contains a number of utilites for generating GitLab Code Quality reports from industry standard tools.

## Semgrep

The Semgrep analyser runs a bunch of preloaded Semgrep rules against your codebase.

## `sarif2gitlab`

Small CLI utility for converting a generic SARIF report into the GitLab format.

Usage:

```bash
wget -O sarif2gitlab https://gitlab.com/av1o/gitlab-cq/-/package_files/69700636/download
chmod +x sarif2gitlab
./sarif2gitlab --input file.sarif --output gl-sast-report.json 
```
