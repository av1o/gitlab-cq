.PHONEY: semgrep
semgrep:
	docker build -t cq .
	docker build -t cq/semgrep --build-arg=BASE_SRC=cq -f dockerfiles/semgrep.Dockerfile .
	docker run --network=none -it --entrypoint sh -v $(PWD):/workspace cq/semgrep