FROM registry.gitlab.com/av1o/base-images/go-git:1.21 as BASE

RUN mkdir -p /home/somebody/go && \
    mkdir -p /home/somebody/.tmp

WORKDIR /home/somebody/go
RUN mkdir -p bin

# copy our code in
COPY --chown=somebody:0 . .

# build the binary
RUN for dir in ./cmd/*/; do \
    echo "Building $(basename "$dir")" && \
    TMPDIR=/home/somebody/.tmp \
    CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o "bin/$(basename "$dir")" -buildmode=pie "./cmd/$(basename "$dir")"; \
    done


FROM harbor.dcas.dev/docker.io/returntocorp/semgrep:1.36.0

RUN adduser -S somebody --uid 1001 -G root

# copy the compiled binary from the builder
COPY --chown=somebody:0 --from=BASE /home/somebody/go/bin/semgrep /usr/local/bin/cq

# download rules from the semgrep registry
RUN mkdir -p /rules && \
    wget https://semgrep.dev/c/p/default -O /rules/default.yaml && \
    wget https://semgrep.dev/c/p/secrets -O /rules/secrets.yaml && \
    wget https://semgrep.dev/c/p/gitleaks -O /rules/gitleaks.yaml && \
    wget https://semgrep.dev/c/p/trailofbits -O /rules/trailofbits.yaml && \
    wget https://semgrep.dev/c/p/security-audit -O /rules/security-audit.yaml && \
    wget https://semgrep.dev/c/p/insecure-transport -O /rules/insecure-transport.yaml && \
    wget https://semgrep.dev/c/p/command-injection -O /rules/command-injection.yaml && \
    wget https://semgrep.dev/c/p/sql-injection -O /rules/sql-injection.yaml && \
    wget https://semgrep.dev/c/p/kubernetes -O /rules/kubernetes.yaml && \
    wget https://semgrep.dev/c/p/terraform -O /rules/terraform.yaml && \
    wget https://semgrep.dev/c/p/golang -O /rules/golang.yaml && \
    wget https://semgrep.dev/c/p/gosec -O /rules/gosec.yaml && \
    wget https://semgrep.dev/c/p/java -O /rules/java.yaml && \
    wget https://semgrep.dev/c/p/findsecbugs -O /rules/findsecbugs.yaml && \
    wget https://semgrep.dev/c/p/c -O /rules/c.yaml && \
    wget https://semgrep.dev/c/p/flawfinder -O /rules/flawfinder.yaml && \
    wget https://semgrep.dev/c/p/react -O /rules/react.yaml && \
    wget https://semgrep.dev/c/p/docker -O /rules/docker.yaml && \
    wget https://semgrep.dev/c/p/dockerfile -O /rules/dockerfile.yaml && \
    wget https://semgrep.dev/c/p/kotlin -O /rules/kotlin.yaml && \
    wget https://semgrep.dev/c/p/python -O /rules/python.yaml && \
    wget https://semgrep.dev/c/p/bandit -O /rules/bandit.yaml && \
    wget https://semgrep.dev/c/p/typescript -O /rules/typescript.yaml && \
    wget https://semgrep.dev/c/p/javascript -O /rules/javascript.yaml && \
    wget https://semgrep.dev/c/p/nodejs -O /rules/nodejs.yaml && \
    wget https://semgrep.dev/c/p/csharp -O /rules/csharp.yaml && \
    wget https://semgrep.dev/c/p/security-code-scan -O /rules/security-code-scan.yaml && \
    wget https://semgrep.dev/c/p/brakeman -O /rules/brakeman.yaml && \
    wget https://semgrep.dev/c/p/r2c-security-audit -O /rules/r2c-security-audit.yaml && \
    wget https://semgrep.dev/c/p/r2c-bug-scan -O /rules/r2c-bug-scan.yaml && \
    wget https://semgrep.dev/c/p/r2c-best-practices -O /rules/r2c-best-practices.yaml && \
    wget https://semgrep.dev/c/p/cwe-top-25 -O /rules/cwe-top-25.yaml && \
    wget https://semgrep.dev/c/p/owasp-top-ten -O /rules/owasp-top-ten.yaml && \
    wget https://semgrep.dev/c/p/gitlab -O /rules/gitlab.yaml && \
    wget https://semgrep.dev/c/p/xss -O /rules/xss.yaml

RUN chown -R somebody:0 /rules && \
    chmod g=u -R /rules

USER somebody
WORKDIR /

ENV SEMGREP_URL=https://localhost
ENV SEMGREP_APP_URL=https://localhost

ENTRYPOINT ["/bin/sh"]
