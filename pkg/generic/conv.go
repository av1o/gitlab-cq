/*
 *    Copyright 2022 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package generic

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"github.com/owenrumney/go-sarif/sarif"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/gitlab-cq/pkg/gitlab"
	"strconv"
	"strings"
)

// AsGitLab converts a SARIF report
// into one supported by GitLab Code Quality.
func AsGitLab(report *sarif.Report) []gitlab.Report {
	//goland:noinspection GoPreferNilSlice
	reports := []gitlab.Report{}
	for _, r := range report.Runs {
	out:
		for _, res := range r.Results {
			// check if the result has been suppressed
			if len(res.Suppressions) > 0 {
				for _, s := range res.Suppressions {
					if s.Status != nil {
						// if the status isn't approved then we still need to show the
						// finding
						//
						// https://gitlab.com/gitlab-org/gitlab/-/issues/344616#note_862455506
						if *s.Status != "underReview" && *s.Status != "rejected" {
							log.Debugf("suppressing finding with status: %s", *s.Status)
							continue out
						}
						log.Debugf("ignoring suppression with status: %s", *s.Status)
					} else {
						log.Debugf("suppressing finding: %s", s.Kind)
						continue out
					}
				}
			}
			for _, loc := range res.Locations {
				// skip possibly invalid items
				if loc.PhysicalLocation == nil || loc.PhysicalLocation.Region == nil || loc.PhysicalLocation.ArtifactLocation == nil {
					continue
				}
				// append the report
				reports = append(reports, gitlab.Report{
					Description: description(res),
					Fingerprint: fingerprint(res, loc),
					Severity:    getSeverity(strOrEmpty(res.Level)),
					Location: gitlab.Location{
						Path: strOrEmpty(loc.PhysicalLocation.ArtifactLocation.URI),
						Lines: gitlab.Lines{
							Begin: intOrEmpty(loc.PhysicalLocation.Region.StartLine),
						},
					},
				})
			}
		}
	}
	return reports
}

func description(r *sarif.Result) string {
	id := strOrEmpty(r.RuleID)
	desc := strOrEmpty(r.Message.Text)
	if id == "" {
		return desc
	}
	return fmt.Sprintf("%s: %s", id, desc)
}

func fingerprint(r *sarif.Result, l *sarif.Location) string {
	h := sha256.New()
	h.Write([]byte(strings.Join([]string{
		strOrEmpty(r.RuleID), strOrEmpty(l.PhysicalLocation.ArtifactLocation.URI), strconv.Itoa(intOrEmpty(l.PhysicalLocation.Region.StartLine)),
	}, "")))
	return hex.EncodeToString(h.Sum(nil))
}

// getSeverity converts SARIF levels
// into gitlab-compatible versions.
//
// Tried to work off the info found in the SARIF
// spec: https://docs.oasis-open.org/sarif/sarif/v2.0/sarif-v2.0.html
func getSeverity(level string) string {
	switch level {
	default:
		fallthrough
	case "note":
		return "info"
	case "error":
		return "critical"
	case "warning":
		return "major"
	}
}

func strOrEmpty(s *string) string {
	if s == nil {
		return ""
	}
	return *s
}

func intOrEmpty(s *int) int {
	if s == nil {
		return 0
	}
	return *s
}
