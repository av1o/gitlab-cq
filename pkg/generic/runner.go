/*
 *    Copyright 2022 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package generic

import (
	"context"
	"github.com/owenrumney/go-sarif/sarif"
	log "github.com/sirupsen/logrus"
	"gitlab.com/autokubeops/cuttle/pkg/runtime"
	"gitlab.com/av1o/gitlab-cq/internal/core"
	"os"
	"path/filepath"
)

func RunSARIF(fileRaw, fileOutput, cmd string, args ...string) error {
	// start the runtime
	rt := runtime.NewRuntime(cmd)
	err := rt.Execute(context.TODO(), args...)
	if err != nil {
		log.WithError(err).Error("failed to execute")
		return err
	}
	report, err := sarif.Open(fileRaw)
	if err != nil {
		log.WithError(err).Error("failed to read raw report")
		return err
	}
	glRep := AsGitLab(report)
	if err := core.WriteReports(glRep, filepath.Join(os.TempDir(), fileOutput)); err != nil {
		log.WithError(err).Error("failed to write report to file")
		return err
	}
	return nil
}
