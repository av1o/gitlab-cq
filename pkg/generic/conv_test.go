package generic

import (
	"github.com/owenrumney/go-sarif/sarif"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"strings"
	"testing"
)

func TestAsGitLab(t *testing.T) {
	log.SetLevel(log.DebugLevel)
	t.Run("reports are parsed", func(t *testing.T) {
		sarifReport, err := sarif.Open("./testdata/semgrep.sarif")
		require.NoError(t, err)

		reports := AsGitLab(sarifReport)
		assert.NotEmpty(t, reports)
		assert.Len(t, reports, 1)

		first := reports[0]
		assert.True(t, strings.HasPrefix(first.Description, "bandit.B101-1: "))
	})
	t.Run("suppressed reports are ignored", func(t *testing.T) {
		sarifReport, err := sarif.Open("./testdata/semgrep_suppressed.sarif")
		require.NoError(t, err)

		reports := AsGitLab(sarifReport)
		assert.Empty(t, reports)
	})
	t.Run("suppressed reports under review are parsed", func(t *testing.T) {
		sarifReport, err := sarif.Open("./testdata/semgrep_suppressed_invalid.sarif")
		require.NoError(t, err)

		reports := AsGitLab(sarifReport)
		assert.NotEmpty(t, reports)
		assert.Len(t, reports, 1)

		first := reports[0]
		assert.True(t, strings.HasPrefix(first.Description, "bandit.B101-1: "))
	})
	t.Run("mixed reports are parsed", func(t *testing.T) {
		sarifReport, err := sarif.Open("./testdata/semgrep_suppressed_mixed.sarif")
		require.NoError(t, err)

		reports := AsGitLab(sarifReport)
		assert.NotEmpty(t, reports)
		assert.Len(t, reports, 1)

		first := reports[0]
		assert.True(t, strings.HasPrefix(first.Description, "bandit.B101-1: "))
	})
}
