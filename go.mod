module gitlab.com/av1o/gitlab-cq

go 1.20

require (
	github.com/owenrumney/go-sarif v1.1.1
	github.com/sirupsen/logrus v1.9.3
	github.com/stretchr/testify v1.8.4
	gitlab.com/autokubeops/cuttle v0.0.0-20211130062832-7b5cb649a7ca
)

require (
	github.com/apparentlymart/go-textseg/v13 v13.0.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/zclconf/go-cty v1.13.2 // indirect
	golang.org/x/sys v0.11.0 // indirect
	golang.org/x/text v0.12.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
